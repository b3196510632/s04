// Activity
const names = {
  Brandon: {
    name: "Brandon Boyd",
    age: 35,
  },
  Steve: {
    name: "Steve Tyler",
    age: 56,
  },
};

const users = [
  {
    username: "brBoyd87",
    password: "87brandon19",
  },
  {
    username: "tylerofsteve",
    password: "stevenstyle75",
  },
];

function factorial(number) {
  if (typeof number !== "number") return undefined;
  if (number < 0) return undefined;
  if (number === 0) return 1;
  if (number === 1) return 1;
  return number * factorial(number - 1);
}

module.exports = {
  names: names,
  users: users,
  factorial: factorial,
};
